package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;


public class BannedPlayersMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_banned_players";

    public static final BasicMetric INSTANCE = new BannedPlayersMetric();

    private static final Gauge GAUGE = Gauge.build().name(NAME).help("The amount of banned players.").create();

    private BannedPlayersMetric()
    {
    }

    @Override
    public void update()
    {
        GAUGE.set(Bukkit.getBannedPlayers().size());
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
