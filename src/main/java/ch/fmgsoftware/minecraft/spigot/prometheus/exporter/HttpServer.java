package ch.fmgsoftware.minecraft.spigot.prometheus.exporter;


import fi.iki.elonen.NanoHTTPD;

import java.io.IOException;
import java.io.StringWriter;
import java.util.concurrent.ExecutionException;


public class HttpServer
    extends NanoHTTPD
{
    private final String m_path;
    private final MetricsCollector m_metricsCollector;

    public HttpServer(int port, String path, MetricsCollector metricsCollector)
    {
        super(port);

        if(path == null)
        {
            throw new IllegalArgumentException("The path cannot be null!");
        }

        if(metricsCollector == null)
        {
            throw new IllegalArgumentException("The metrics collector cannot be null!");
        }

        m_path = path.startsWith("/") ? path : ("/" + path);
        m_metricsCollector = metricsCollector;
    }

    @Override
    public Response serve(IHTTPSession session)
    {
        if(session.getMethod() != Method.GET)
        {
            return newFixedLengthResponse(Response.Status.BAD_REQUEST, MIME_PLAINTEXT, "Method not supported!");
        }

        if(!session.getUri().equals(m_path))
        {
            return newFixedLengthResponse(Response.Status.NOT_FOUND, MIME_PLAINTEXT, "not found");
        }

        StringWriter writer = new StringWriter();

        try
        {
            m_metricsCollector.writeTo(writer);
        }
        catch(IOException | InterruptedException | ExecutionException exception)
        {
            return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, MIME_PLAINTEXT, exception.toString());
        }

        return newFixedLengthResponse(Response.Status.OK, MIME_PLAINTEXT, writer.toString());
    }
}
