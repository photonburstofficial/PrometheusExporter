package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class AutoSaveMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_auto_save";

    public static final WorldMetric INSTANCE = new AutoSaveMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("If the world has auto save enabled 1; otherwise 0.")
        .labelNames("world")
        .create();

    private AutoSaveMetric()
    {
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.isAutoSave() ? 1 : 0);
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
