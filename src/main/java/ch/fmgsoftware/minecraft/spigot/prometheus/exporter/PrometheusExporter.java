package ch.fmgsoftware.minecraft.spigot.prometheus.exporter;


import ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics.*;
import io.prometheus.client.hotspot.DefaultExports;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;


public class PrometheusExporter
    extends JavaPlugin
{
    private HttpServer m_httpServer;

    @Override
    public void onEnable()
    {
        super.onEnable();

        saveDefaultConfig();

        FileConfiguration config = getConfig();

        if(config.getBoolean("metrics.groups.jvm", true))
        {
            getLogger().info(() -> "JVM metrics activated");
            DefaultExports.initialize();
        }

        MetricsCollector metricsCollector = new MetricsCollector(this);

        if(config.getBoolean("metrics.groups.basics", true))
        {
            metricsCollector.activate(TicksPerSecondMetric.INSTANCE);
            metricsCollector.activate(LoadedChunksMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.players", true))
        {
            metricsCollector.activate(MaxPlayersMetric.INSTANCE);
            metricsCollector.activate(OnlinePlayersMetric.INSTANCE);
            metricsCollector.activate(OperatorPlayersMetric.INSTANCE);
            metricsCollector.activate(PlayersTotalMetric.INSTANCE);
            metricsCollector.activate(WorldPlayersMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.entities", true))
        {
            metricsCollector.activate(EntitiesMetric.INSTANCE);
            metricsCollector.activate(LivingEntitiesMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.environments", false))
        {
            metricsCollector.activate(WorldsMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.plugins", false))
        {
            metricsCollector.activate(LoadedPluginsMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.bans", false))
        {
            metricsCollector.activate(BannedIPsMetric.INSTANCE);
            metricsCollector.activate(BannedPlayersMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.weather", false))
        {
            metricsCollector.activate(StormMetric.INSTANCE);
            metricsCollector.activate(ThunderingMetric.INSTANCE);
            metricsCollector.activate(WeatherDurationMetric.INSTANCE);
            metricsCollector.activate(ThunderDurationMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.worldTime", false))
        {
            metricsCollector.activate(WorldTimeMetric.INSTANCE);
            metricsCollector.activate(WorldFullTimeMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.onlineMode", false))
        {
            metricsCollector.activate(OnlineModeMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.difficulty", false))
        {
            metricsCollector.activate(DifficultyMetric.INSTANCE);
        }

        if(config.getBoolean("metrics.groups.autoSave", false))
        {
            metricsCollector.activate(AutoSaveMetric.INSTANCE);
        }

        m_httpServer = new HttpServer(
            getConfig().getInt("http.port", 9123),
            getConfig().getString("http.path", "/metrics"),
            metricsCollector);

        try
        {
            m_httpServer.start();
        }
        catch(IOException exception)
        {
            getLogger().severe(exception.toString());
        }
    }

    @Override
    public void onDisable()
    {
        super.onDisable();

        m_httpServer.stop();
    }
}
