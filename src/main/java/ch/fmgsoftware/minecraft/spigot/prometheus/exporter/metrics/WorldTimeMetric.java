package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class WorldTimeMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_game_time";

    public static final WorldMetric INSTANCE = new WorldTimeMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The current game time in each world.")
        .labelNames("world")
        .create();

    private WorldTimeMetric()
    {
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.getTime());
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
