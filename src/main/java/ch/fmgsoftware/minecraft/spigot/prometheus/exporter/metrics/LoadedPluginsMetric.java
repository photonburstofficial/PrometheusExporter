package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;


public class LoadedPluginsMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_loaded_plugins";

    public static final BasicMetric INSTANCE = new LoadedPluginsMetric();

    public static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The loaded plugins and if they are enabled.")
        .labelNames("name")
        .create();

    private LoadedPluginsMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update()
    {
        for(Plugin plugin : Bukkit.getPluginManager().getPlugins())
        {
            GAUGE.labels(plugin.getName()).set(plugin.isEnabled() ? 1 : 0);
        }
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
