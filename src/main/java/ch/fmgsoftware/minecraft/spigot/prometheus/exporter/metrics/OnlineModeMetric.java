package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;


public class OnlineModeMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_online_mode";

    public static final BasicMetric INSTANCE = new OnlineModeMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("If the server is in online mode 1; otherwise 0.")
        .create();

    private OnlineModeMetric()
    {
    }

    @Override
    public void update()
    {
        GAUGE.set(Bukkit.getOnlineMode() ? 1 : 0);
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
