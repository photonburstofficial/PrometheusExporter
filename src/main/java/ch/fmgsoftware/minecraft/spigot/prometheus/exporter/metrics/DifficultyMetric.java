package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Difficulty;
import org.bukkit.World;


public class DifficultyMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_difficulty";

    public static final WorldMetric INSTANCE = new DifficultyMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The world difficulty setting (0: peaceful, 1: easy, 2: normal, 3: hard, NaN: unknown).")
        .labelNames("world")
        .create();

    private static double convertDifficulty(Difficulty difficulty)
    {
        switch(difficulty)
        {
        case PEACEFUL:
            return 0;
        case EASY:
            return 1;
        case NORMAL:
            return 2;
        case HARD:
            return 3;
        default:
            return Double.NaN;
        }
    }

    private DifficultyMetric()
    {
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(convertDifficulty(world.getDifficulty()));
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
