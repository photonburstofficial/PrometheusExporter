package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;


public class MaxPlayersMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_max_players";

    public static final BasicMetric INSTANCE = new MaxPlayersMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The maximum players that can play on this server.")
        .create();

    private MaxPlayersMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update()
    {
        GAUGE.set(Bukkit.getMaxPlayers());
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
