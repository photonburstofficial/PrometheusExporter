package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


public interface Metric
{
    void register();

    String getName();
}
