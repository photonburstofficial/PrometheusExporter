package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;


public class WorldsMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_worlds";

    public static final BasicMetric INSTANCE = new WorldsMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The amount of worlds on the server.")
        .labelNames("environment")
        .create();

    private final HashMap<World.Environment, Integer> m_worldCounter;

    private WorldsMetric()
    {
        m_worldCounter = new HashMap<>();
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update()
    {
        m_worldCounter.clear();

        for(World world : Bukkit.getWorlds())
        {
            m_worldCounter.merge(world.getEnvironment(), 1, (a, b) -> a + b);
        }

        for(Map.Entry<World.Environment, Integer> worlds : m_worldCounter.entrySet())
        {
            GAUGE.labels(worlds.getKey().name()).set(worlds.getValue());
        }
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
