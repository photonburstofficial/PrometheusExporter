package ch.fmgsoftware.minecraft.spigot.prometheus.exporter;


import ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics.BasicMetric;
import ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics.Metric;
import ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics.WorldMetric;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.common.TextFormat;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


public class MetricsCollector
{
    private final JavaPlugin m_plugin;

    private final ArrayList<BasicMetric> m_basicMetrics;
    private final ArrayList<WorldMetric> m_worldMetrics;

    public MetricsCollector(JavaPlugin plugin)
    {
        if(plugin == null)
        {
            throw new IllegalArgumentException("The plugin cannot be null!");
        }

        m_plugin = plugin;

        m_basicMetrics = new ArrayList<>();
        m_worldMetrics = new ArrayList<>();
    }

    public void activate(BasicMetric metric)
    {
        m_basicMetrics.add(metric);
        register(metric);
    }

    public void activate(WorldMetric metric)
    {
        m_worldMetrics.add(metric);
        register(metric);
    }

    public void writeTo(Writer writer)
        throws IOException, ExecutionException, InterruptedException
    {
        Future future = m_plugin.getServer().getScheduler().callSyncMethod(m_plugin, () -> {

            for(BasicMetric metric : m_basicMetrics)
            {
                metric.update();
            }

            for(World world : Bukkit.getWorlds())
            {
                for(WorldMetric metric : m_worldMetrics)
                {
                    metric.update(world);
                }
            }

            return null;
        });

        future.get();

        TextFormat.write004(writer, CollectorRegistry.defaultRegistry.metricFamilySamples());
    }

    private void register(Metric metric)
    {
        metric.register();
        m_plugin.getLogger().info(() -> "Metric activated: " + metric.getName());
    }
}
