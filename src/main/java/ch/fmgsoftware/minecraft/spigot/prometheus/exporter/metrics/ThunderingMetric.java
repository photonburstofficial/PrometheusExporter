package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class ThunderingMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_thundering";

    public static final WorldMetric INSTANCE = new ThunderingMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("If it is thundering in the world 1; otherwise 0.")
        .labelNames("world")
        .create();

    private ThunderingMetric()
    {
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.isThundering() ? 1 : 0);
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
