package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class LivingEntitiesMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_living_entities";

    public static final WorldMetric INSTANCE = new LivingEntitiesMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The amount of living entities on the server.")
        .labelNames("world")
        .create();

    private LivingEntitiesMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.getLivingEntities().size());
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
