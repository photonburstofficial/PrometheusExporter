package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.Bukkit;


public class BannedIPsMetric
    implements BasicMetric
{
    public static final String NAME = "minecraft_server_banned_ips";

    public static final BasicMetric INSTANCE = new BannedIPsMetric();

    private static final Gauge GAUGE = Gauge.build().name(NAME).help("The amount of banned IPs").create();

    private BannedIPsMetric()
    {
    }

    @Override
    public void update()
    {
        GAUGE.set(Bukkit.getIPBans().size());
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
