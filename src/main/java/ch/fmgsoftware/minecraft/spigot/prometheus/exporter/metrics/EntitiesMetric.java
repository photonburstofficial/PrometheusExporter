package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class EntitiesMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_entities";

    public static final WorldMetric INSTANCE = new EntitiesMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The amount of entities on the server.")
        .labelNames("world")
        .create();

    private EntitiesMetric()
    {
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.getEntities().size());
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
