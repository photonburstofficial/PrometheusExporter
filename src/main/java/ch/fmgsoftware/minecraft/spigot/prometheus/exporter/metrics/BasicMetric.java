package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


public interface BasicMetric
    extends Metric
{
    void update();
}
