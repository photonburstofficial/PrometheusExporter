package ch.fmgsoftware.minecraft.spigot.prometheus.exporter.metrics;


import io.prometheus.client.Gauge;
import org.bukkit.World;


public class ThunderDurationMetric
    implements WorldMetric
{
    public static final String NAME = "minecraft_server_thunder_remaining_ticks";

    public static final WorldMetric INSTANCE = new ThunderDurationMetric();

    private static final Gauge GAUGE = Gauge.build()
        .name(NAME)
        .help("The remaining ticks for the current thunderstorm.")
        .labelNames("world")
        .create();

    private ThunderDurationMetric()
    {
    }

    @Override
    public void update(World world)
    {
        GAUGE.labels(world.getName()).set(world.getThunderDuration());
    }

    @Override
    public void register()
    {
        GAUGE.register();
    }

    @Override
    public String getName()
    {
        return NAME;
    }
}
